local oil = require("oil")

oil.setup {
    keymaps = {
        ["g?"] = { "actions.show_help", mode = "n" },
        ["<CR>"] = "actions.select",
        ["<C-s>"] = false,
        ["<C-h>"] = false,
        ["<C-t>"] = false,
    },
    view_options = {
        show_hidden = true,
    }
}
